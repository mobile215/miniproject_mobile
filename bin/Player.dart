import 'package:chalkdart/chalk.dart';

class Player {
  String name = "";
  int lose = 0;
  int win = 0;

  Player() {
    lose = 0;
    win = 0;
  }
  void setName(String name) {
    this.name = name;
  }

  int getLost() {
    return lose;
  }

  int getWin() {
    return win;
  }

  void addWin() {
    win++;
  }

  void addLose() {
    lose++;
  }

  String toString() {
    return (chalk.blue.bold("$name ") + "Win: $win Lose: $lose \n");
  }
}
