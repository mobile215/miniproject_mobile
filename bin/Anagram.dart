import 'dart:io';
import 'dart:math';
import 'package:chalkdart/chalk.dart';

import 'Game.dart';
import 'Hint.dart';
import 'ListWord.dart';
import 'Player.dart';

class Anagram extends Game implements Hint {
  List<String> randomListWord = [];
  List<String> historyResult = [];
  List<String> shuffleWord = [];
  String first = "";
  String randomWord = "";
  ListWord listWord = ListWord();
  Player player = Player();
  int hintCount = 1;
  bool finish = false;
  @override
  void showWelcome() {
    print(chalk.yellow.bold("\n===================================================================================================================================\n"));
    print(chalk.green("""

▒█░░▒█ ▒█▀▀▀ ▒█░░░ ▒█▀▀█ ▒█▀▀▀█ ▒█▀▄▀█ ▒█▀▀▀ 　 ▀▀█▀▀ ▒█▀▀▀█ 　 ░█▀▀█ ▒█▄░▒█ ░█▀▀█ ▒█▀▀█ ▒█▀▀█ ░█▀▀█ ▒█▀▄▀█ 　 ▒█▀▀█ ░█▀▀█ ▒█▀▄▀█ ▒█▀▀▀ 
▒█▒█▒█ ▒█▀▀▀ ▒█░░░ ▒█░░░ ▒█░░▒█ ▒█▒█▒█ ▒█▀▀▀ 　 ░▒█░░ ▒█░░▒█ 　 ▒█▄▄█ ▒█▒█▒█ ▒█▄▄█ ▒█░▄▄ ▒█▄▄▀ ▒█▄▄█ ▒█▒█▒█ 　 ▒█░▄▄ ▒█▄▄█ ▒█▒█▒█ ▒█▀▀▀ 
▒█▄▀▄█ ▒█▄▄▄ ▒█▄▄█ ▒█▄▄█ ▒█▄▄▄█ ▒█░░▒█ ▒█▄▄▄ 　 ░▒█░░ ▒█▄▄▄█ 　 ▒█░▒█ ▒█░░▀█ ▒█░▒█ ▒█▄▄█ ▒█░▒█ ▒█░▒█ ▒█░░▒█ 　 ▒█▄▄█ ▒█░▒█ ▒█░░▒█ ▒█▄▄▄
   
    """));
  }

  @override
  void showRules() {
    print(chalk.red.bold("Rules:"));
    print("1.All word first is will set by anagram game.");
    print("2.Word must be at least three letters in length.");
    print("3.Hint is will only one.");
    print("4.You can only enter answer 5 times to win the anagram game.");
    print(chalk.yellow.bold(
        "\n===================================================================================================================================\n"));
  }

  void showHistoryResult() {
    print("HIST: $historyResult");
  }

  void showFirstChar() {
    if (randomListWord.length != 0) {
      first = randomListWord[0][0];
      print("First word of anagram game is: " + chalk.blue.bold("$first"));
    }
  }

  void showRemainingWord() {
    //โชร์คำเหลือ
    print("remaining word: ${randomListWord.length}");
  }

  void showRandomWord() {
    stdout.write("[ ");
    for (var i = 0; i < randomWord.length; i++) {
      stdout.write("${randomWord[i].toUpperCase()} ");
      if (i != randomWord.length - 1) {
        stdout.write(", ");
      }
    }
    stdout.write("] \n");
  }

  bool checkResult() {
    //เช็คคำตอบถูกผิด
    int limit = 5;
    int count = 0;
    //  int count = 0;
    while (true) {
     

      if (count >= limit) {
        return false;
      }
      stdout.write("\nPlease enter your answer (or '/' to hint): ");
      String input = stdin.readLineSync()!;
      if (input == "/") {
        hint();
        continue;
      }
       count += 1;
      if (randomListWord.length == 0) {
        return true;
      } else if (randomListWord.contains(input.toLowerCase())) {
        //ถูก ลบคำตอบเดิมแล้วรับค่าใหม่n
        print(chalk.green.bold("\n$input is correct"));
        print(chalk.yellow.bold("====================================================\n"));
        historyResult.add(input.toUpperCase());
        randomListWord.remove(input.toLowerCase());
        hintCount = 1;

        if (randomListWord.length == 0) {
          return true;
        }
        showHistoryResult();
        showRemainingWord();
        showFirstChar();
        showRandomWord();
        continue;
      }
      if (!randomListWord.contains(input.toLowerCase()) && count < limit) {
        //ผิด ให้รับค่าใหม่
        print(chalk.red.bold("\n$input not correct!! Please enter your new answer"));
        print(chalk.yellow.bold("====================================================\n"));
        showHistoryResult();
        showRemainingWord();
        showFirstChar();
        showRandomWord();
        continue;
      }
    }
  }

  void showRandomListWord() {
    print("Answer is: " + chalk.blue.bold("${randomListWord}\n"));
  }

  String randomCharacter(String temp) {
    //สุ่มคำแรก
    return (temp.split('')..shuffle()).join();
  }

  void pickRandomWord() {
    //เลือกคำสุ่ม
    String temp = listWord.mixWord();
    randomWord = temp;
    suffleWords();
  }

  void prepareListWord() {
    listWord.randomList("anagram");
    randomListWord = listWord.getRandomListWord();
  }

  void suffleWords() {
    //สลับคำ
    for (var i = 0; i < randomWord.length; i++) {
      shuffleWord.add(randomWord[i]);
    }
    shuffleWord.shuffle();
    randomWord = "";
    for (var i = 0; i < shuffleWord.length; i++) {
      randomWord += shuffleWord[i];
    }
  }

  void resetGame() {
    historyResult = []; //รีเซตปรวัติคำตอบ
    randomListWord = []; //รีเซ็ตการสุ่มหมวดคำ
    shuffleWord = []; //รีเซ็ตการสลับคำ
    hintCount = 1;
  }

  void playagain() {
    //เล่นเกมอีกครั้ง
    while (true) {
      print("Do you want to play again?(y,n)");
      String ans = stdin.readLineSync()!;
      if (ans == "y") {
        resetGame();
        finish = false;
        break;
      } else if (ans == "n") {
        print("Thank you for play anagram game^^");
        resetGame();
        finish = true;
        break;
      } else {
        continue;
      }
    }
    print("\x1B[2J\x1B[0;0H");
  }

  void setPlayer(Player player) {
    this.player = player;
  }

  void play() {
    while (finish != true) {
      showWelcome();
      showRules();
      showHistoryResult();
      prepareListWord(); //เซ็ตการสุ่มคำของเกม ลิตคำใหม่
      showRemainingWord();
      showFirstChar();
      pickRandomWord(); //เลือกสุ่มคำใหม่
      showRandomWord();
      bool check = checkResult();

      if (check == true) {
        print(chalk.green.bold("YOU WIN!!!\n"));
        player.addWin();
        print(player);
        playagain();
      } else {
        print(chalk.yellow.bold("====================================================\n"));
        print(chalk.red.bold("YOU LOSE;-;\n"));
        showRandomListWord();
        player.addLose();
        print(player);
        playagain();
      }
    }
    finish = false;
  }

  @override
  void hint() {
    Random random = Random();
    int positionHint = random.nextInt(randomListWord[0].length);
    if (positionHint == 0) { //สุ่มตำแหน่งใบ้
      positionHint++;
    }
    if (hintCount > 0) { //ใบ้ได้
      hintCount--; //ลดคำใบ้
      for (var i = 0; i < randomListWord[0].length; i++) {
        if (i == 0) {
          stdout.write("${randomListWord[0][0]} ");
        } else {
          if (i == positionHint) {
            stdout.write("${randomListWord[0][i]} ");
          } else {
            stdout.write("_ ");
          }
        }
      }

    } else {
      print(chalk.yellow.bold("can't hint!!"));

    }
  }
}
