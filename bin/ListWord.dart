import 'dart:math';

import 'word.dart';

class ListWord {
  Random random = Random();
  List<String> randomListWord = []; //สุ่มหมวดคำ
  List<String> historyResult = []; //ประวัติคำตอบ
  List<String> shuffleWord = []; //สลับคำ?
  String suffleWord = ""; //สุ่มคำทั้งหมด
  var AllListWord = Word().getListWordAnagram(); //หมวดคำF
  String word = "";

  String mixWord() {
    //รวมคำในคำศัพท์ของเกมทั้งหมด
    String temp = "";
    for (int i = 0; i < randomListWord.length; i++) {
      for (var j = 0; j < randomListWord[i].length; j++) {
        if (!temp.contains(randomListWord[i][j])) {
          temp += randomListWord[i][j];
        }
      }
    }
    return temp;
  }

  void suffle() {
    //สลับคำ
    for (var i = 0; i < suffleWord.length; i++) {
      shuffleWord.add(suffleWord[i]);
    }
    shuffleWord.shuffle();
    suffleWord = "";
    for (var i = 0; i < shuffleWord.length; i++) {
      suffleWord += shuffleWord[i];
    }
  }

  List<String> getRandomListWord() {
    return randomListWord;
  }

  List<String> getSuffleWord() {
    return shuffleWord;
  }

  void randomList(String game) {
    randomListWord = [];
    //เซ็ตการสุ่มคำของเกม ลิตคำใหม่
    random = Random();
    List<String> temp = [];
    if (game == "anagram") {
      AllListWord = Word().getListWordAnagram();
      int randomNumber = random.nextInt(AllListWord.length);
      temp = AllListWord[randomNumber]; //ทำให้เป็นตัวเล็กให้หมด
      temp.forEach((element) {
        randomListWord.add(element.toLowerCase());
      });
    } else {
      AllListWord = Word().getListWordSynonym();
      int randomNumber = random.nextInt(AllListWord.length);
      temp = AllListWord[randomNumber]; //ทำให้เป็นตัวเล็กให้หมด
      temp.forEach((element) {
        randomListWord.add(element.toLowerCase());
      });
    }
  }
}
