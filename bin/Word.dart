class Word {
  List<List<String>> listWordAnagram = [
    ["FOR", "FORM", "FROM"],
    ["PAINO", "PAN", "PIN", "PAIN"],
    ["WIT", "WHIT", "WITH"],
    ["SIT", "SIN", "SAINT"],
    ["BOW", "BLOW", "BOWL"],
    ["SEA", "SAKE", "SPEAK"],
    ["COD", "CLOUD", "COLD", "COULD"],
    ["FIG", "FIT", "FIGHT"],
    ["CAT", "COT", "COAT"],
    ["FAIR", "FAR", "FRAY", "FAIRY"],
    ["TANK", "THAN", "THANK"],
    ["BIG", "BRIG", "BRING"],
    ["FOND", "FUN", "FUND", "FOUND"],
    ["CAR", "CAVE", "CAVER", "CARE"],
    ["WEND", "WORE", "WONDER"],
  ];

  List<List<String>> listWordSynonym = [
    ["GOOD", "FINE"],
    ["PRETTY", "LOVELY"],
    ["START", "BEGIN"],
    ["ALWAYS", "FOREVER"],
    ["LAST", "FINAL"],
    ["EASY", "SIMPLE"],
    ["FALSE", "UNTRUE"],
    ["EXIT", "LEAVE"],
    ["JOIN", "CONNECT"],
    ["ANSWER", "REPLY"],
    ["AMAZING", "WONDERFUL"],
    ["SCARED", "AFRAID"],
    ["SHOW", "DISPLAY"],
    ["TRUE", "CORRECT"],
    ["SMALL", "TINY"],
  ];

  List<List<String>> getListWordAnagram() {
    return listWordAnagram;
  }

  List<List<String>> getListWordSynonym() {
    return listWordSynonym;
  }
}
