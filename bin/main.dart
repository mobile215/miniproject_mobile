import 'Anagram.dart';
import 'Game.dart';
import 'dart:io';

import 'Player.dart';
import 'Synonym.dart';

void main(List<String> arguments) {
  var anagram = Anagram();
  var synonym = Synonym();
  var game = Game();
  var player = Player();

  game.showWelcome();
  game.showRules();
  game.showName(player);
  game.showselect();

  while (true) {
    stdout.write("Please select game: ");
    String x = stdin.readLineSync()!;
    if (x == "1") {
      anagram.setPlayer(player);
      anagram.play();
    } else if (x == "2") {
      synonym.setPlayer(player);
      synonym.initialGame();
      synonym.play();
    } else if (x == "3") {
      break;
    }
  }
}
