import 'dart:io';
import 'dart:math';
import 'package:chalkdart/chalk.dart';

import 'Game.dart';
import 'Hint.dart';
import 'ListWord.dart';
import 'Player.dart';

class Synonym extends Game implements Hint {
  Player player = Player();
  ListWord listWord = ListWord();
  List<String> randomListWord = [];
  String word = "";
  String synonymWord = "";
  String input = "";
  bool isFinish = false;
  int remaining = 5;
  String hintWord = "";
  int hintCount = 0;

  void showWelcome() {
    print(chalk.yellow.bold( "\n===================================================================================================================================\n"));
    print(chalk.green("""
     
▒█░░▒█ ▒█▀▀▀ ▒█░░░ ▒█▀▀█ ▒█▀▀▀█ ▒█▀▄▀█ ▒█▀▀▀ 　 ▀▀█▀▀ ▒█▀▀▀█ 　 ▒█▀▀▀█ ▒█░░▒█ ▒█▄░▒█ ▒█▀▀▀█ ▒█▄░▒█ ▒█░░▒█ ▒█▀▄▀█ 　 ▒█▀▀█ ░█▀▀█ ▒█▀▄▀█ ▒█▀▀▀ 
▒█▒█▒█ ▒█▀▀▀ ▒█░░░ ▒█░░░ ▒█░░▒█ ▒█▒█▒█ ▒█▀▀▀ 　 ░▒█░░ ▒█░░▒█ 　 ░▀▀▀▄▄ ▒█▄▄▄█ ▒█▒█▒█ ▒█░░▒█ ▒█▒█▒█ ▒█▄▄▄█ ▒█▒█▒█ 　 ▒█░▄▄ ▒█▄▄█ ▒█▒█▒█ ▒█▀▀▀ 
▒█▄▀▄█ ▒█▄▄▄ ▒█▄▄█ ▒█▄▄█ ▒█▄▄▄█ ▒█░░▒█ ▒█▄▄▄ 　 ░▒█░░ ▒█▄▄▄█ 　 ▒█▄▄▄█ ░░▒█░░ ▒█░░▀█ ▒█▄▄▄█ ▒█░░▀█ ░░▒█░░ ▒█░░▒█ 　 ▒█▄▄█ ▒█░▒█ ▒█░░▒█ ▒█▄▄▄

    """));
  }

  void showRules() {
    print(chalk.red.bold("Rules:"));
    print("1.Fill in the correct words.");
    print("2.Hint is will set by Synonym game.");
    print("3.You can only enter answer 5 times to win the Synonym game.");
    print(chalk.yellow.bold("\n===================================================================================================================================\n"));
  }

  void setPlayer(Player player) {
    this.player = player;
  }

  @override
  void hint() {
    List tempHint = []; //history hint index
    suffleIndex(tempHint);

    if (hintCount > 0) {
      hintCount--;
      for (var i = 0; i < synonymWord.length; i++) {
        if (i == tempHint.first) {
          hintWord =
              hintWord.replaceRange(i, i + 1, synonymWord[tempHint.first]);
          tempHint.removeAt(0);
          break;
        } else {
          // stdout.write("_ ");
        }
      }
    } else {
      print(chalk.yellow.bold("can't hint!!"));
    }
  }

  void suffleIndex(List<dynamic> tempHint) {
    for (int i = 0; i < randomListWord[0].length; i++) {
      tempHint.add(i);
    }
    tempHint.shuffle();
  }

  void play() {
    while (isFinish == false) {
      prepareListWord();
      pickWord();
      processLine();
      showWelcome();
      showRules();

      while (true) {
        showRemaining();
        showHint();
        showWord(); //แสดงคำศัพท์
        showLine(); //แสดงเส้นใต้ของคำศัพท์
        takeInput(); //รับ input
        if (checkInput() == true) {
          player.addWin();
          //เช็คคำตอบ
          print(chalk.green.bold("\n$synonymWord is correct"));
          print(chalk.yellow.bold("====================================================\n"));
          print(chalk.green.bold("YOU WIN!!!\n"));
          //print("You Win!!!\n");
          print(player);
          if (playagain() == true) {
            //ถ้าถูกให้เริ่มเล่นใหม่ที่ลูปนอก
            print(player);
            initialGame();
            break;
          } else {
            //ถ้าผู้เล่นไม่เล่น break เพื่อออกจาก loop ใน และ isFinish = false เพื่อออกจาก loop นอก
            isFinish = true;
            break;
          }
        } else if (checkInput() == false) {
          if (input == "/") {
            hint();
          } else {
            print(chalk.red.bold("\n$input is not correct"));
            //คำตอบผิดให้ลด count ลง และวน loop ในซ้ำ
            remaining--;
          }
          print(chalk.yellow.bold("====================================================\n"));
        }
        if (remaining == 0) {
          //หมดโควต้าการเล่น
          player.addLose();
          print("Answer is: " + chalk.blue.bold("$synonymWord"));
          print(chalk.red.bold("YOU LOSE;-;\n"));
          //print("You lose;-;\n");
          print(player);
          if (playagain() == true) {
            //ถามผู้เล่นว่าต้องการเล่นอีกไหม ถ้าเล่นจะ break ไปเริ่มต้นที่ loop นอก
            initialGame();
            break;
          } else {
            //ถ้าผู้เล่นไม่เล่น break เพื่อออกจาก loop ใน และ isFinish = false เพื่อออกจาก loop นอก
            isFinish = true;
            break;
          }
        }
      }
      print("\x1B[2J\x1B[0;0H");
    }
  }

  void showHint() {
    print("Hint: $hintCount");
  }

  void takeInput() {
    stdout.write("\nPlease enter your answer (or '/' to hint): ");
    input = stdin.readLineSync()!;
    input = input.toLowerCase();
  }

  void processLine() {
    for (var i = 0; i < synonymWord.length; i++) {
      hintWord += "_";
    }
  }

  void showLine() {
    for (var i = 0; i < synonymWord.length; i++) {
      stdout.write("${hintWord[i]} ");
    }
    print("");
  }

  void showWord() {
    print("\n[ ${word.toUpperCase()} ]");
  }

  void prepareListWord() {
    listWord.randomList("synonym");
    randomListWord = listWord.getRandomListWord();
  }

  void pickWord() {
    Random random = Random();
    int index = random.nextInt(randomListWord.length);
    word = randomListWord[index]; //เก็บคำศัพท์
    if (index == 0) {
      //เก็บคำความหมายเหมือน
      synonymWord = randomListWord[1];
    } else {
      synonymWord = randomListWord[0];
    }
    if (synonymWord.length >= 4) {
      hintCount = (synonymWord.length * 0.5).floor();
    }
  }

  bool checkInput() {
    if (input == synonymWord) {
      return true;
    } else {
      return false;
    }
  }

  bool playagain() {
    //เล่นเกมอีกครั้ง
    while (true) {
      print("Do you want to play again?(y,n)");
      String ans = stdin.readLineSync()!;
      if (ans == "y") {
        return true;
      } else if (ans == "n") {
        print("Thank you for play anagram game^^");
        return false;
      } else {
        continue;
      }
    }
  }

  void showRemaining() {
    print("Ramaining: $remaining");
  }

  void initialGame() {
    isFinish = false;
    hintWord = ""; //รีเซตปรวัติคำตอบ
    remaining = 5; //รีเซ็ตการสุ่มหมวดคำ
  }
}
